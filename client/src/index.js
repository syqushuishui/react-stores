// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Alert } from './widgets';
import { studentStore } from './stores';

// Reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let script = document.createElement('script');
  script.src = '/reload/reload.js';
  if (document.body) document.body.appendChild(script);
}

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

class Menu extends Component {
  render() {
    return (
      <table>
        <tbody>
          <tr>
            <td>
              <NavLink activeStyle={{ color: 'darkblue' }} exact to="/">
                React example
              </NavLink>
            </td>
            <td>
              <NavLink activeStyle={{ color: 'darkblue' }} to="/students">
                Students
              </NavLink>
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

class Home extends Component {
  render() {
    return <div>React example with component state</div>;
  }
}

class StudentList extends Component {
  render() {
    return (
      <ul>
        {studentStore.students.map(student => (
          <li key={student.email}>
            <NavLink activeStyle={{ color: 'darkblue' }} exact to={'/students/' + student.id}>
              {student.firstName} {student.lastName}
            </NavLink>{' '}
            <NavLink activeStyle={{ color: 'darkblue' }} to={'/students/' + student.id + '/edit'}>
              edit
            </NavLink>
          </li>
        ))}
      </ul>
    );
  }

  mounted() {
    studentStore.getStudents().catch((error: Error) => Alert.danger(error.message));
  }
}

class StudentDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    return (
      <div>
        <ul>
          <li>First name: {studentStore.currentStudent.firstName}</li>
          <li>Last name: {studentStore.currentStudent.lastName}</li>
          <li>Email: {studentStore.currentStudent.email}</li>
        </ul>
      </div>
    );
  }

  mounted() {
    studentStore.getStudent(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }
}

class StudentEdit extends Component<{ match: { params: { id: number } } }> {
  render() {
    return (
      <form>
        <ul>
          <li>
            First name:{' '}
            <input
              type="text"
              value={studentStore.currentStudent.firstName}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (studentStore.currentStudent.firstName = event.target.value)
              }
            />
          </li>
          <li>
            Last name:{' '}
            <input
              type="text"
              value={studentStore.currentStudent.lastName}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (studentStore.currentStudent.lastName = event.target.value)
              }
            />
          </li>
          <li>
            Email:{' '}
            <input
              type="text"
              value={studentStore.currentStudent.email}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (studentStore.currentStudent.email = event.target.value)
              }
            />
          </li>
        </ul>
        <button type="button" onClick={this.save}>
          Save
        </button>
      </form>
    );
  }

  mounted() {
    studentStore.getStudent(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }

  save() {
    studentStore
      .updateStudent()
      .then(() => history.push('/students/' + studentStore.currentStudent.id))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={Home} />
        <Route path="/students" component={StudentList} />
        <Route exact path="/students/:id" component={StudentDetails} />
        <Route exact path="/students/:id/edit" component={StudentEdit} />
      </div>
    </HashRouter>,
    root
  );
